/*
 ******************************************************************************
 * *** LCD ***
 * PC10  - RS
 * PC11  - E
 * PC12  - D4
 * PD2   - D5
 * PC8   - D6
 * PC9   - D7 marron - cn
 *
 * *** ADC ***
 * PC1
 *
 * *** BOTON CAMBIO BANDA (rojo) ***
 * PA3
 *
 * *** BOTON CAMBIO ONDA (verde) ***
 * PC13
 *
 * *** BOTON CAMBIO ATENUACION (violeta) ***
 * PC0
 *
 * *** SALIDA DAC ***
 * PA5
 *
 * *** LEDS ***
 * Amarillo: Alimentacion
 * Verde: ATT1 (PB0)
 * Azul: ATT2 (PB11)
 * Violeta: ATT3 (PB10)
 * Gris: Cambio banda 1 (PB9)
 * Blanco: Cambio banda 2 (PB8)
 *
 ******************************************************************************
 */

#include"stm32f4xx_gpio.h"
#include"stm32f4xx_dac.h"
#include"stm32f4xx_rcc.h"
#include"stm32f4xx_tim.h"
#include"stm32f4xx_it.h"
#include"stm32f4xx_dma.h"
#include "stm32_ub_lcd_2x16.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#define DAC_DHR12R2_ADDRESS   0x40007414
#define VOLT_REF	2950
#define bufferFreqMax 5

void INIT_ALL(void);

/* Private function prototypes -----------------------------------------------*/
void Imprimir_LCD(void);
void CHANGE_FREQ();
static void TIM6_Config(void);
static void DAC_Ch2_SineWaveConfig(void);
void Delay(uint32_t tiempo);
void CHANGE_WAVEFORM(void);
void DAC_TRIANG(void);
void DAC_SINE(void);
void DAC_CUAD(void);
void DAC_SINE_MITAD(void);
void DAC_SINE_CUARTO(void);
void DAC_TRIANG_MITAD(void);
void DAC_TRIANG_CUARTO(void);
void DAC_CUAD_MITAD(void);
void DAC_CUAD_CUARTO(void);
void VOLT_TO_FREQ(float volt);
void EXTI_LINE13_INIT(void);
void EXTI_LINE3_INIT(void);

void PRINT_LCD(void);
void PRINT_WAVEFORM(void);
void PRINT_FREQ(void);

void INIT_LEDS(void);

void CHANGE_BAND(void);
void CHANGE_ATT(void);

char fila2[16];

// Control vars
uint32_t waveform = 0;
uint8_t att = 0;

// Interruption ack flag
uint8_t flagSwitch = 0;
uint8_t flagAdc = 0;
uint8_t flagBand = 0;
uint32_t flagLCD = 0;
uint8_t flagAten = 0;

float volt = 0.0f;
float freq = 0.0f;
float freqFifo[bufferFreqMax];
float freqSum = 0.0f;
uint8_t freqQueue = 0;

uint16_t PrescalerValue = 0;

void TIM2_CONFIG(void);

void ADC_INIT(void);
void TIM3_CONFIG(void);
int32_t ADC_READ(void);
float ADC_VOLT(void);

DAC_InitTypeDef DAC_InitStructure;

char fila1[16];
char fila2[16];

uint16_t i = 0;
uint8_t band = 1;

uint16_t aSine12bit[128] = { 2048, 2140, 2233, 2325, 2416, 2506, 2596, 2684,
		2770, 2855, 2937, 3018, 3096, 3171, 3244, 3314, 3381, 3444, 3504, 3561,
		3614, 3663, 3708, 3749, 3785, 3818, 3846, 3870, 3889, 3904, 3914, 3920,
		3921, 3918, 3910, 3897, 3880, 3859, 3833, 3802, 3767, 3729, 3686, 3639,
		3588, 3533, 3475, 3413, 3348, 3279, 3208, 3134, 3057, 2978, 2896, 2812,
		2727, 2640, 2551, 2461, 2370, 2279, 2186, 2094, 2001, 1909, 1816, 1725,
		1634, 1544, 1455, 1368, 1283, 1199, 1117, 1038, 961, 887, 816, 747, 682,
		620, 562, 507, 456, 409, 366, 328, 293, 262, 236, 215, 198, 185, 177,
		174, 175, 181, 191, 206, 225, 249, 277, 310, 346, 387, 432, 481, 534,
		591, 651, 714, 781, 851, 924, 999, 1077, 1158, 1240, 1325, 1411, 1499,
		1589, 1679, 1770, 1862, 1955, 2047 };

uint16_t aCuadrada12bit[128] = { 174, 233, 293, 352, 412, 471, 531, 590, 650,
		709, 769, 828, 887, 947, 1006, 1066, 1125, 1185, 1244, 1304, 1363, 1423,
		1482, 1542, 1601, 1661, 1720, 1780, 1839, 1899, 1958, 2018, 2077, 2137,
		2196, 2256, 2315, 2375, 2434, 2494, 2553, 2613, 2672, 2732, 2791, 2851,
		2910, 2970, 3029, 3089, 3148, 3208, 3267, 3326, 3386, 3445, 3505, 3564,
		3624, 3683, 3743, 3802, 3862, 3802, 3743, 3683, 3624, 3564, 3505, 3445,
		3386, 3326, 3267, 3208, 3148, 3089, 3029, 2970, 2910, 2851, 2791, 2732,
		2672, 2613, 2553, 2494, 2434, 2375, 2315, 2256, 2196, 2137, 2077, 2018,
		1958, 1899, 1839, 1780, 1720, 1661, 1601, 1542, 1482, 1423, 1363, 1304,
		1244, 1185, 1125, 1066, 1006, 947, 887, 828, 769, 709, 650, 590, 531,
		471, 412, 352, 293, 233 };

uint16_t aTriangular12bit[128] = { 3921, 3921, 3921, 3921, 3921, 3921, 3921,
		3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921,
		3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921,
		3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921,
		3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921,
		3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 3921, 174, 174, 174,
		174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174,
		174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174,
		174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174,
		174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174, 174,
		174, 174, 174, 174, 3921 };

uint16_t aSenoidal12BitMitad[128];
uint16_t aTriangular12BitMitad[128];
uint16_t aCuadrada12BitMitad[128];
uint16_t aSenoidal12BitCuarto[128];
uint16_t aTriangular12BitCuarto[128];
uint16_t aCuadrada12BitCuarto[128];

void CALC_MITAD_SENO(void);
void CALC_MITAD_TRIANG(void);
void CALC_MITAD_CUAD(void);
void CALC_CUARTO_SENO(void);
void CALC_CUARTO_TRIANG(void);
void CALC_CUARTO_CUAD(void);

TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

int main(void) {
	// Initialize all hardware, timers and functionalities
	INIT_ALL();

	GPIO_SetBits(GPIOB, GPIO_Pin_0);
	while (1) {
		if (flagSwitch == 1) {
			flagSwitch = 0;
			CHANGE_WAVEFORM();
		} else if (flagLCD > 500) {
			flagLCD = 0;
			PRINT_LCD();
		} else if (flagAten == 1) {
			flagAten = 0;
			CHANGE_ATT();
		} else if (flagAdc == 1) {
			flagAdc = 0;
			VOLT_TO_FREQ(ADC_VOLT());
		} else if (flagBand == 1) {
			flagBand = 0;
			CHANGE_BAND();
		}
	}
}

void EXTI0_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line0) != RESET) {

		flagAten = 1;
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}

void EXTI15_10_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line13) != RESET) {
		flagSwitch = 1;

		EXTI_ClearITPendingBit(EXTI_Line13);
	}
}

void EXTI3_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line3) != RESET) {
		flagBand = 1;

		EXTI_ClearITPendingBit(EXTI_Line3);
	}
}

void TIM3_IRQHandler(void) {
	if (TIM_GetITStatus(TIM3, TIM_IT_CC1) != RESET) {
		flagAdc = 1;
		flagLCD++;
		TIM_ClearITPendingBit(TIM3, TIM_IT_CC1);
	}
}

void PRINT_LCD(void) {
	UB_LCD_2x16_Clear();

	PRINT_WAVEFORM();

	PRINT_FREQ();
}

void PRINT_WAVEFORM() {
	if (waveform == 0)
		UB_LCD_2x16_String(0, 0, "    SENOIDAL");
	else if (waveform == 1)
		UB_LCD_2x16_String(0, 0, "    CUADRADA");
	else if (waveform == 2)
		UB_LCD_2x16_String(0, 0, "   TRIANGULAR");
}

void PRINT_FREQ() {
	freqFifo[freqQueue] = freq;

	if (freqQueue < bufferFreqMax)
		freqQueue++;
	else {
		for (uint8_t i = 0; i < bufferFreqMax; i++)
			freqFifo[i] = 0x00;

		freqQueue = 0;
	}

	for (uint8_t i = 0; i < freqQueue; i++)
		freqSum += freqFifo[i];

	if (freqQueue != 0)
		sprintf(fila2, "    %.1f Hz", freqSum / freqQueue);

	freqSum = 0.0f;

	UB_LCD_2x16_String(0, 1, fila2);
}

void CHANGE_WAVEFORM(void) {
	waveform++;
	Delay(300);
	if (waveform >= 3)
		waveform = 0;

	if (att == 0) {
		if (waveform == 0)
			DAC_Ch2_SineWaveConfig();
		else if (waveform == 1)
			DAC_TRIANG();
		else if (waveform == 2)
			DAC_CUAD();
	} else if (att == 1) {
		if (waveform == 0)
			DAC_SINE_MITAD();
		else if (waveform == 1)
			DAC_TRIANG_MITAD();
		else if (waveform == 2)
			DAC_CUAD_MITAD();
	} else if (att == 2) {
		if (waveform == 0)
			DAC_SINE_CUARTO();
		else if (waveform == 1)
			DAC_TRIANG_CUARTO();
		else if (waveform == 2)
			DAC_CUAD_CUARTO();
	}
}

void CHANGE_FREQ() {
	// Desactivamos el TIM6
	TIM_Cmd(TIM6, DISABLE);

	// Actualizamos el valor de TIM_Period
	TIM_TimeBaseStructure.TIM_Period = (uint16_t) 1 / ((1 / 90e6) * freq * 128)
			- 1;

	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);

	// Activamos el TIM6
	TIM_Cmd(TIM6, ENABLE);
}

void CHANGE_ATT() {
	att++;
	Delay(300);

	if (att >= 3)
		att = 0;

	if (waveform == 0) {
		if (att == 0) {
			DAC_Ch2_SineWaveConfig();
			GPIO_SetBits(GPIOB, GPIO_Pin_0);
			GPIO_ResetBits(GPIOB, GPIO_Pin_10);
			GPIO_ResetBits(GPIOB, GPIO_Pin_11);
		} else if (att == 1) {
			DAC_SINE_MITAD();
			GPIO_ResetBits(GPIOB, GPIO_Pin_0);
			GPIO_SetBits(GPIOB, GPIO_Pin_10);
			GPIO_ResetBits(GPIOB, GPIO_Pin_11);
		} else if (att == 2) {
			DAC_SINE_CUARTO();
			GPIO_ResetBits(GPIOB, GPIO_Pin_0);
			GPIO_ResetBits(GPIOB, GPIO_Pin_10);
			GPIO_SetBits(GPIOB, GPIO_Pin_11);
		}
	} else if (waveform == 1) {
		if (att == 0) {
			DAC_TRIANG();
			GPIO_SetBits(GPIOB, GPIO_Pin_0);
			GPIO_ResetBits(GPIOB, GPIO_Pin_10);
			GPIO_ResetBits(GPIOB, GPIO_Pin_11);
		} else if (att == 1) {
			DAC_TRIANG_MITAD();
			GPIO_ResetBits(GPIOB, GPIO_Pin_0);
			GPIO_SetBits(GPIOB, GPIO_Pin_10);
			GPIO_ResetBits(GPIOB, GPIO_Pin_11);
		} else if (att == 2) {
			DAC_TRIANG_CUARTO();
			GPIO_ResetBits(GPIOB, GPIO_Pin_0);
			GPIO_ResetBits(GPIOB, GPIO_Pin_10);
			GPIO_SetBits(GPIOB, GPIO_Pin_11);
		}
	} else if (waveform == 2) {
		if (att == 0) {
			DAC_CUAD();
			GPIO_SetBits(GPIOB, GPIO_Pin_0);
			GPIO_ResetBits(GPIOB, GPIO_Pin_10);
			GPIO_ResetBits(GPIOB, GPIO_Pin_11);
		} else if (att == 1) {
			DAC_CUAD_MITAD();
			GPIO_ResetBits(GPIOB, GPIO_Pin_0);
			GPIO_SetBits(GPIOB, GPIO_Pin_10);
			GPIO_ResetBits(GPIOB, GPIO_Pin_11);
		} else if (att == 2) {
			DAC_CUAD_CUARTO();
			GPIO_ResetBits(GPIOB, GPIO_Pin_0);
			GPIO_ResetBits(GPIOB, GPIO_Pin_10);
			GPIO_SetBits(GPIOB, GPIO_Pin_11);
		}
	}
}

void VOLT_TO_FREQ(float volt) {
	if (volt >= 0.1) {
		if (band == 0)
			freq = (float) volt * 100;
		else if (band == 1)
			freq = (float) volt * 1000;
		else
			freq = 0;
	} else {
		if (band == 0)
			freq = 30;
		else if (band == 1)
			freq = 300;
		else
			freq = 0;
	}

	CHANGE_FREQ();
}

void CHANGE_BAND(void) {
	if (band == 1)
	{
		band = 0;
		GPIO_SetBits(GPIOB, GPIO_Pin_8);
		GPIO_ResetBits(GPIOB, GPIO_Pin_9);
	}
	else
	{
		band = 1;
		GPIO_SetBits(GPIOB, GPIO_Pin_9);
		GPIO_ResetBits(GPIOB, GPIO_Pin_8);
	}
}

void CALC_MITAD_SENO(void) {
	for (uint8_t i = 0; i < 128; i++) {
		aSenoidal12BitMitad[i] = aSine12bit[i] * 2048 / 4095;
	}
}

void CALC_MITAD_TRIANG(void) {
	for (uint8_t i = 0; i < 128; i++) {
		aTriangular12BitMitad[i] = aTriangular12bit[i] * 2048 / 4095;
	}
}

void CALC_MITAD_CUAD(void) {
	for (uint8_t i = 0; i < 128; i++) {
		aCuadrada12BitMitad[i] = aCuadrada12bit[i] * 2048 / 4095;
	}
}

void CALC_CUARTO_SENO(void) {
	for (uint8_t i = 0; i < 128; i++) {
		aSenoidal12BitCuarto[i] = aSine12bit[i] * 1024 / 4095;
	}
}

void CALC_CUARTO_TRIANG(void) {
	for (uint8_t i = 0; i < 128; i++) {
		aTriangular12BitCuarto[i] = aTriangular12bit[i] * 1024 / 4095;
	}
}

void CALC_CUARTO_CUAD(void) {
	for (uint8_t i = 0; i < 128; i++) {
		aCuadrada12BitCuarto[i] = aCuadrada12bit[i] * 1024 / 4095;
	}
}

static void TIM6_Config(void) {
	/* TIM6 Periph clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);

	/* --------------------------------------------------------
	 TIM6 input clock (TIM6CLK) is set to 2 * APB1 clock (PCLK1),
	 since APB1 prescaler is different from 1.
	 TIM6CLK = 2 * PCLK1
	 TIM6CLK = HCLK / 2 = SystemCoreClock /2

	 TIM6 Update event occurs each TIM6CLK/280

	 Para 32 puntos en memoria:
	 1                                         1
	 f = ------------------------------------------------ = ------------------------------
	 [1/(SystemCoreClock /2)]*(TIM_PERIOD+1)*muestras	 (1/90x10^6)*(TIM_PERIOD+1)*32

	 Para que f=750Hz  -> TIM_Period = 3749
	 Para que f=2.5kHz -> TIM_Period = 1124

	 Note:
	 SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f4xx.c file.
	 Each time the core clock (HCLK) changes, user had to call SystemCoreClockUpdate()
	 function to update SystemCoreClock variable value. Otherwise, any configuration
	 based on this variable will be incorrect.

	 ----------------------------------------------------------- */

	/* Time base configuration */
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = (uint16_t) 1 / ((1 / 90e6) * 2500 * 128)
			- 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);

	/* TIM6 TRGO selection */
	TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);

	/* TIM6 enable counter */
	TIM_Cmd(TIM6, ENABLE);
}

static void DAC_Ch2_SineWaveConfig(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aSine12bit;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_SINE_MITAD(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aSenoidal12BitMitad;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_SINE_CUARTO(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aSenoidal12BitCuarto;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void Delay(uint32_t tiempo) {
	while (--tiempo)
		;
}

void DAC_TRIANG(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aTriangular12bit;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_TRIANG_MITAD(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aTriangular12BitMitad;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_TRIANG_CUARTO(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aTriangular12BitCuarto;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_CUAD(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aCuadrada12bit;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_CUAD_MITAD(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aCuadrada12BitMitad;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void DAC_CUAD_CUARTO(void) {
	DMA_InitTypeDef DMA_InitStructure;

	/* DAC channel2 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable; //NOTA! el buffer habilitado deforma la se�al generada(ver)
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);

	/* DMA1_Stream6 channel7 configuration **************************************/
	DMA_DeInit(DMA1_Stream6);
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) DAC_DHR12R2_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &aCuadrada12BitCuarto;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = 128;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	/* Enable DMA1_Stream6 */
	DMA_Cmd(DMA1_Stream6, ENABLE);

	/* Enable DAC Channel2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	/* Enable DMA for DAC Channel2 */
	DAC_DMACmd(DAC_Channel_2, ENABLE);
}

void EXTI_LINE13_INIT(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Activamos el reloj para GPIOC */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	/* Activamos el reloj para SYSCFG */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Configuramos el pin PC13 como salida */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Conectamos el pin PC13 a la interrupcion EXTI Line13 */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);

	/* Configuramos la interrrupcion EXTI Line13 */
	EXTI_InitStructure.EXTI_Line = EXTI_Line13;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Activamos y seteamos la interrupcion EXTI Line13 para la prioridad mas alta */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void EXTI_LINE3_INIT(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Activamos el reloj para GPIOA */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	/* Activamos el reloj para SYSCFG */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Configuramos el pin PC13 como salida */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Conectamos el pin PC13 a la interrupcion EXTI Line13 */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource3);

	/* Configuramos la interrrupcion EXTI Line13 */
	EXTI_InitStructure.EXTI_Line = EXTI_Line3;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Activamos y seteamos la interrupcion EXTI Line13 para la prioridad mas alta */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void EXTI_LINE0_INIT(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Activamos el reloj para GPIOA */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	/* Activamos el reloj para SYSCFG */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Configuramos el pin PC13 como salida */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Conectamos el pin PC13 a la interrupcion EXTI Line13 */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource0);

	/* Configuramos la interrrupcion EXTI Line13 */
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Activamos y seteamos la interrupcion EXTI Line13 para la prioridad mas alta */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void ADC_INIT(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

	/* Puerto C -------------------------------------------------------------*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	/* PC1 para entrada analogica */
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Activar ADC1 ----------------------------------------------------------*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	/* ADC Common Init -------------------------------------------------------*/
	ADC_CommonStructInit(&ADC_CommonInitStructure);
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div4; // max 36 MHz segun datasheet
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	/* ADC Init ---------------------------------------------------------------*/
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(ADC1, &ADC_InitStructure);

	/* Establecer la configuraci�n de conversi�n ------------------------------*/
	ADC_InjectedSequencerLengthConfig(ADC1, 1);
	ADC_SetInjectedOffset(ADC1, ADC_InjectedChannel_1, 0);
	ADC_InjectedChannelConfig(ADC1, ADC_Channel_11, 1,
	ADC_SampleTime_480Cycles);

	/* Poner en marcha ADC ----------------------------------------------------*/
	ADC_Cmd(ADC1, ENABLE);
}

int32_t ADC_READ(void) {

	uint32_t valor_adc;

	ADC_ClearFlag(ADC1, ADC_FLAG_JEOC);      // borrar flag de fin conversion

	ADC_SoftwareStartInjectedConv(ADC1);    // iniciar conversion

	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_JEOC) == RESET)
		; // Espera fin de conversion

	valor_adc = ADC_GetInjectedConversionValue(ADC1, ADC_InjectedChannel_1); // obtiene Valor A-D

	return valor_adc;
}

float ADC_VOLT(void) {
	return 3 * (float) ADC_READ() / 4095;
}

void TIM3_CONFIG(void) {
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	/* Enable the TIM3 gloabal Interrupt */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* -----------------------------------------------------------------------
	 Configuracion TIM3 : Output Compare Timing Mode:

	 IMPORTANTE: el TIM3 toma su base de tiempo del APB1 Timer clock
	 -----------------------------------------------------------------------

	 Para 32 puntos en memoria:
	 1                                         1
	 f = ------------------------------------------------ = ------------------------------
	 [1/(SystemCoreClock /2)]*(TIM_PERIOD+1)*muestras	 (1/90x10^6)*(TIM_PERIOD+1)*32

	 Para que f=750Hz  -> TIM_Period = 3749
	 Para que f=2.5kHz -> TIM_Period = 1124


	 -----------------------------------------------------------------------
	 */
	SystemCoreClockUpdate();
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = (uint16_t) 1 / ((1 / 90e6) * 1 * 32) - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* TIM Interrupts enable */
	TIM_ITConfig(TIM3, TIM_IT_CC1, ENABLE);

	/* TIM3 enable counter */
	TIM_Cmd(TIM3, ENABLE);
}

void TIM2_CONFIG(void) {
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	/* Enable the TIM3 gloabal Interrupt */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* -----------------------------------------------------------------------
	 Configuracion TIM3 : Output Compare Timing Mode:

	 IMPORTANTE: el TIM3 toma su base de tiempo del APB1 Timer clock
	 -----------------------------------------------------------------------

	 Para 32 puntos en memoria:
	 1                                         1
	 f = ------------------------------------------------ = ------------------------------
	 [1/(SystemCoreClock /2)]*(TIM_PERIOD+1)*muestras	 (1/90x10^6)*(TIM_PERIOD+1)*32

	 Para que f=750Hz  -> TIM_Period = 3749
	 Para que f=2.5kHz -> TIM_Period = 1124


	 -----------------------------------------------------------------------
	 */
	SystemCoreClockUpdate();
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = (uint16_t) 1 / ((1 / 90e6) * 1 * 32) - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 1000;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	/* TIM Interrupts enable */
	TIM_ITConfig(TIM2, TIM_IT_CC1, ENABLE);

	/* TIM3 enable counter */
	TIM_Cmd(TIM2, ENABLE);
}

void INIT_LEDS(void) {
	GPIO_InitTypeDef GPIO_InitStructure; //Estructura de configuracion

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	//Ahora se configura el pin PB8

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; //Salida
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	GPIO_Init(GPIOB, &GPIO_InitStructure);//Se aplica la configuraci�n definida anteriormente
}

void INIT_ALL(void) {
	//Fuerza a que tome la configuracion del CLOCK de system_stm32f4.c
	SystemInit();

	// Inicializa el Display
	UB_LCD_2x16_Init();

	GPIO_InitTypeDef GPIO_InitStructure;

	/* DMA1 clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);

	//DAC: activar clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

	//******* CONFIGURAMOS EL PIN DEL uP****************
	// Clock Enable
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// Configuramos el pin 5 como analogica
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	//**************************************************
	/* If the User Button is pressed */

	/* ADC Configuration ------------------------------------------------------*/
	ADC_INIT();

	/* TIM6 Configuration ------------------------------------------------------*/
	TIM6_Config();

	/* TIM6 Configuration ------------------------------------------------------*/
	TIM3_CONFIG();

	/* Sine Wave generator -----------------------------------------------*/
	DAC_Ch2_SineWaveConfig();

	EXTI_LINE13_INIT();
	EXTI_LINE3_INIT();
	EXTI_LINE0_INIT();

	INIT_LEDS();

	CALC_MITAD_SENO();
	CALC_MITAD_TRIANG();
	CALC_MITAD_CUAD();
	CALC_CUARTO_SENO();
	CALC_CUARTO_TRIANG();
	CALC_CUARTO_CUAD();
}
